﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создать сотрудника
        /// </summary>
        /// <param name="employee">Данные о новом сотруднике</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> Create(EmployeeSimpleCreate employee)
        {
            var allRoles = _roleRepository.GetAllAsync().Result;

            List<Role> roles = allRoles.Where(x => employee.RoleId.Contains(x.Id)).ToList();

            return await GetEmployeeByIdAsync(_employeeRepository.Create(employee.Employee(roles)).Result.Id);
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <param name="id">Идентификатор сотрудника для удаления</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            bool exists = await _employeeRepository.GetByIdAsync(id) != null;
            if (!exists)
            {
                return NotFound();
            }

            var deleted = _employeeRepository.Delete(id).Result;

            if (deleted)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Обновить сотрудника
        /// </summary>
        /// <param name="employee">Данные о обновляемом сотруднике</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<EmployeeResponse>> Update(EmployeeSimpleUpdate employee)
        {
            var allRoles = _roleRepository.GetAllAsync().Result;
            List<Role> roles = allRoles.Where(x => employee.RoleId.Contains(x.Id)).ToList();

            bool exists = await _employeeRepository.GetByIdAsync(employee.Id) != null;
            if (!exists)
            {
                return NotFound();
            }

            var updtedEmployee = await _employeeRepository.Update(employee.Employee(roles));
            return await GetEmployeeByIdAsync(updtedEmployee.Id);
        }
    }
}