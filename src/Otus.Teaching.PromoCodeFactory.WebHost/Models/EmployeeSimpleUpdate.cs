﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeSimpleUpdate : EmployeeSimpleCreate
    {
        public Guid Id { get; set; }

        public Employee Employee(List<Role> roles)
        {
            return new Employee
            {
                Id = this.Id,
                AppliedPromocodesCount = this.AppliedPromocodesCount,
                FirstName = this.FirstName,
                LastName = this.LastName,
                Email = this.Email,
                Roles = roles
            };
        }
    }
}
