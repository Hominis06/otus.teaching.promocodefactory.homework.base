﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeSimpleCreate
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int AppliedPromocodesCount { get; set; }
        public List<Guid> RoleId { get; set; }

        public Employee Employee(List<Role> roles)
        {
            return new Employee
            {
                AppliedPromocodesCount = this.AppliedPromocodesCount,
                FirstName = this.FirstName,
                LastName = this.LastName,
                Email = this.Email,
                Roles = roles
            };
        }
    }
}
