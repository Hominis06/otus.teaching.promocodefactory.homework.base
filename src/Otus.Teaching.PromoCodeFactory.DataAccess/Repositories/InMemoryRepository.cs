﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> Create(T enity)
        {
            if (enity.Id == Guid.Empty)
                enity.Id = Guid.NewGuid();
            ((List<T>)Data).Add(enity);
            return Task.Run(() => Data.Last());
        }

        public Task<bool> Delete(Guid id)
        {
            return Task.Run(() => ((List<T>)Data).Remove(GetByIdAsync(id).Result));
        }

        public Task<T> Update(T entity)
        {
            var listData = (List<T>)Data;
            var updatedRecordIndex = (listData.IndexOf(GetByIdAsync(entity.Id).Result));
            listData[updatedRecordIndex] = entity;
            
            return Task.Run(() => GetByIdAsync(entity.Id));
        }
    }
}